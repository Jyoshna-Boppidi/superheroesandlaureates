//
//  superhero.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Boppidi, Jyoshna. All rights reserved.
//

import Foundation
struct Superheros : Codable {
    var squadName:String
    var homeTown:String
    var formed:Int
    var secretBase:String
    var active:Bool
    var members:[Members]
}
struct Members: Codable{
    var name:String
    var age:Int
    var secretIdentity:String
    var powers:[String]
}
