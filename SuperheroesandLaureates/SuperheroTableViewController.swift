//
//  SuperheroTableViewController.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Gandra, Akhila. All rights reserved.
//

import UIKit

class SuperheroTableViewController: UITableViewController {
    let superherodata = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    var Superhs:[Members] = []
    func displaySuperhs(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        do {
            let decoder:JSONDecoder = JSONDecoder()
            Superhs = try decoder.decode(Superheros.self, from: data!).members
            print(Superhs)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                NotificationCenter.default.post(name: NSNotification.Name("Superheros coming"), object: self.Superhs)
            }
        } catch {
            print(error)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let urlSession = URLSession.shared
        let url = URL(string: superherodata)
        urlSession.dataTask(with: url!, completionHandler: displaySuperhs).resume()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Superhs.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Superheros", for: indexPath)
        // Configure the cell...
        cell.textLabel?.text = "\(Superhs[indexPath.row].name) (aka: \(Superhs[indexPath.row].secretIdentity))"
        var pdetails = Superhs[indexPath.row].powers
        var superpower: String = ""
        for t in stride(from: 0, to: pdetails.count, by: 1){
            if t < pdetails.count - 1{
                superpower = superpower + pdetails[t] + ", "
            }
            else{
                superpower = superpower + pdetails[t]
            }
        }
        cell.detailTextLabel?.text = "\(superpower)"
        return cell
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
