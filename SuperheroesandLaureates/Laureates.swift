//
//  Laureates.swift
//  SuperheroesandLaureates
//
//  Created by Student on 4/13/19.
//  Copyright © 2019 Gandra, Akhila. All rights reserved.
//

import Foundation
struct Laureates: Codable{
    var id: String?
    var firstname:String?
    var surname:String?
    var born: String?
    var died:String?
    var bornCountry:String?
    var bornCountryCode: String?
    var bornCity:String?
    var diedCountry:String?
    var diedCountryCode: String?
    var diedCity:String?
    var gender:String?
    var prizes:[Prizes]?
    init( firstname:String?,surname:String?,born:String?,died:String?){
        self.firstname = firstname
        self.surname = surname
        self.born = born
        self.died = died
        self.id =  ""
        self.bornCountry = ""
        self.bornCountryCode=""
        self.bornCity = ""
        self.diedCountry = ""
        self.diedCountryCode = ""
        self.diedCity = ""
        self.gender = ""
        self.prizes = []
    }
}
struct Prizes:Codable{
    var year: String
    var category:String
    var share:String
    var motivation: String
    var affiliations:[Affiliations]
}
struct Affiliations:Codable{
    var name: String
    var city:String
    var country:String
}
